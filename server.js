const express = require ('express');
const bodyParser = require('body-parser')

let app = express();
app.use(bodyParser.json());

let router = require('./routes/routes');

app.listen(3000, function() {
  console.log('Adotmob app listening on port 3000!');
});

router(app);

