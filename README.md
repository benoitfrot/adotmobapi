API : Technical Test for Adotmob
================================

##Set-up

Clone this repository, navigate in it, and run `npm start` from your shell

The api is available at `127.0.0.1:3000`

##Description
I developped this small API containig only one route */locate*

You can use it with this body request : 
```json
[
  {
    "lat": 48.8759992,
    "lon": 2.3481253,
    "name": "Arc de triomphe"
  },
  {
    "lat": 48.86,
    "lon": 2.35,
    "name": "Chatelet"
  }
]
```

###Result Analysis
The supposed result were those 
```json

{
	 "Chatelet": {
		 "lat": 42.86,
		 "lon": 2.35,
		 "name": "Chatelet",
		 "impressions": 136407,
		 "clicks": 16350
	 },
	 "Arc de triomphe": {
		 "lat": 48.8759992,
		 "lon": 2.3481253,
		 "name": "Arc de triomphe",
		 "impressions": 63593,
		 "clicks": 7646
	 }
}
```

If you add all the impression and all the clicks you get 223 996 results. In the events.csv file you have 223 996 lines but the first 
one containes the field names, and the last one is empty. In my implementation i chosed to validate the parsed data from events.csv so i 
will not take in account first and last line, i have then 223 994 results.

