let express = require('express');
let controller = require('../controller/LocationController');

module.exports = function (app)
{
	let router = express.Router();
	router.post('/', controller.locate);
	app.use('/location', router);
}


