var Event = require("./Event");

function Location(lat,lon,name)
{
	this.lat = lat;
	this.lon = lon;  
	this.name = name;
	this.impressions = 0;
	this.clicks = 0;
}

Location.prototype.calculateDistanceFrom = function(event) {
	let distance = ((((event.lat - this.lat)**2)+((event.lon - this.lon))**2)**0.5);
	return distance;
}

module.exports = Location;