const Location = require('../model/Location');
const Event = require('../model/Event');
const fs = require('fs');
const csv = require('fast-csv')

exports.locate = function (req,res)
{
	let locations = loadLocation(req.body);
	let stream = fs.createReadStream("./events.csv");
	csv.fromStream(stream, {headers: ["lat","lon","event_type"]})
	   .on("data", function(data){
	   	if (isValidData(data)){	addEvent(data,locations);	}
	   })
	   .on("end", function(){
	        console.log("Ended parsing CSV");
	        res.send(locations);
	   });	 
}


function incrementCountofNearestLocation(locations,event)
{
	if(locations[0].calculateDistanceFrom(event)>locations[1].calculateDistanceFrom(event))
	{
		(event.type==='imp' ? locations[1].impressions ++ : locations[1].clicks ++);
	}
	else
	{
		(event.type==='imp' ? locations[0].impressions ++ : locations[0].clicks ++);
	}
}

function loadLocation (body)
{
	let chatelet = new Location(body[0].lat, body[0].lon,body[0].name);
	let arcDeTriomphe = new Location(body[1].lat, body[1].lon,body[1].name);
	let locations = [];
	locations.push(chatelet);
	locations.push(arcDeTriomphe);
	return locations;
}

function addEvent(data,locations)
{
	let event = new Event(data.lat,data.lon,data.event_type);
	incrementCountofNearestLocation(locations,event);
}
 
function isValidData(data)
{
	return (!isNaN(data.lat) && !isNaN(data.lon) && (data.event_type === "imp" || data.event_type === "click"))
}




